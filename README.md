# Janio Tech Task #1 Going Back In Time #

## Requirements ##
* Linux
* Docker

## Running Program ##
./clock <OFFSET HOURS>

e.g. to go back in time 2 hours


./clock 2

## Running Tests ##
./test
