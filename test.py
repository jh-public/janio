#!/usr/bin/env python3

from clock import Clock
import datetime
import unittest

class TestClock( unittest.TestCase ):

    test_time = datetime.datetime( 2019, 9, 11, 12, 0 )

    def offset_test( self, offset_hours, offset_time ):
        clock_time = Clock( offset_hours=offset_hours ).get_offset_time( self.test_time )
        self.assertEqual( clock_time, offset_time  )

    def test_get_display_time( self ):
        self.assertEqual( 
          Clock( offset_hours=0 ).display_time( self.test_time ),
          '\rWed, 11-Sep-2019 12:00:00 ',
        )
        print('')

    def test_get_offset_time_1_hour( self ):
        self.offset_test( 1, datetime.datetime( 2019, 9, 11, 11, 0 ) )

    def test_get_offset_time_24_hours( self ):
        self.offset_test( 24, datetime.datetime( 2019, 9, 10, 12, 0 ) )

if __name__ == '__main__':
    unittest.main()
