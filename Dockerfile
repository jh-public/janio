FROM python:3.7

RUN date \
    && useradd user \
    && mkdir -p /home/user

USER user

COPY *.py /home/user/

ENTRYPOINT ["python3", "/home/user/clock.py"]

CMD ["-h"]
