#!/usr/bin/python3

from datetime import datetime, timedelta
import argparse
import time

class Clock:

    def __init__( self, offset_hours, time_format=None ):
        self.offset_hours = offset_hours
        self.time_format = time_format or '%a, %d-%b-%Y %H:%M:%S'

    def display_time( self, time ):
        time_string = '\r%s ' % time.strftime( self.time_format )
        print( time_string, end='' )
        return time_string

    def get_offset_time( self, time ):
        return time - timedelta( hours = self.offset_hours )

    def loop( self, interval=1 ):
        while True:
            offset_time = self.get_offset_time( datetime.now() )
            self.display_time( offset_time )
            time.sleep( interval )


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument( 'offset_hours', type=int, help='Hours back in time' )
    parser.add_argument( '-f', '--time_format', help='Time format (Python strftime)' )
    args = parser.parse_args()
    Clock(
        offset_hours=args.offset_hours,
        time_format=args.time_format,
    ).loop()
